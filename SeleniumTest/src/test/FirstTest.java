package test;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class FirstTest {

	public static void main(String[] args) throws MalformedURLException {
	    WebDriver driver = new RemoteWebDriver(
	        new URL("http://127.0.0.1:9515"),
	        new ChromeOptions());
	    driver.get("http://www.google.com");
	    driver.quit();
	  }
	
}
